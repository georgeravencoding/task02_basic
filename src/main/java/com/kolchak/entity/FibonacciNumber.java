package com.kolchak.entity;

/**
 * Importing library Scanner to get input from console.
 */

import java.util.Scanner;

/**
 * Calculating Fibonacci numbers by inputting the count of values in console
 * by user.
 *
 * @author Maksym Kolchak
 * @version 1.0
 */
public class FibonacciNumber {
    /**
     * First Fibonacci number which by default is 0.
     */
    private int firstNumber = 0;
    /**
     * Second Fibonacci number which by default is 1.
     */
    private int secondNumber = 1;
    /**
     * Store the calculated value: first number plus second number.
     */
    private int thirdNumber;
    /**
     * Max odd number.
     */
    private int maxOdd;
    /**
     * Max even number.
     */
    private int maxEven;
    /**
     * Count of odd numbers. By default 1 because of second number.
     */
    private float oddCounter = 1;
    /**
     * Count of even numbers.
     */
    private float evenCounter;
    /**
     * 100% - all count of values.
     */
    private static final int PERCENTAG = 100;

    /**
     * Creating object scanner which give opportunity to input values
     * direct in console.
     */
    private Scanner scanner = new Scanner(System.in);

    /**
     * Calculating Fibonacci numbers by inputting the count of values
     * in console.
     */
    public void showFibonacci() {
        System.out.println("Fibonacci numbers");
        System.out.print("Input count of numbers: ");
        int count = scanner.nextInt();
        System.out.print(firstNumber + " " + secondNumber);
        for (int i = 2; i < count; i++) {
            thirdNumber = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = thirdNumber;
            System.out.print(" " + thirdNumber);
            if (thirdNumber % 2 != 0) {
                maxOdd = thirdNumber;
                oddCounter++;
            } else if (thirdNumber % 2 == 0) {
                maxEven = thirdNumber;
                evenCounter++;
            }
        }
        System.out.println("\nMax odd number is: " + maxOdd);
        System.out.println("Max even number is: " + maxEven);

        float oddPercentage = oddCounter / count * PERCENTAG;
        float evenPercentage = evenCounter / count * PERCENTAG;
        float zeroPercentage = PERCENTAG - oddPercentage - evenPercentage;

//        Is it really make sense to make sout command shorter than
//        80 characters?
        System.out.println(String.format("Percentage of odd numbers is: "
                + "%.2f", oddPercentage));
        System.out.println(String.format("Percentage of even numbers is: "
                + "%.2f", evenPercentage));
        System.out.println(String.format("Percentage of zero value is: "
                + "%.2f", zeroPercentage));

    }
}
