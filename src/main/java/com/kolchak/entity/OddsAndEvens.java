package com.kolchak.entity;

/**
 * Importing library Scanner to get input from console.
 */

import java.util.Scanner;

/**
 * Show odd numbers and even numbers in reverse order between the numbers
 * which are inputted by user in console.
 *
 * @author Maksym Kolchak
 * @version 1.0
 */
public final class OddsAndEvens {

    /**
     * User input the first number in console.
     */
    private int firstNumber;
    /**
     * User input the last number in console.
     */
    private int lastNumber;
    /**
     * Store the sum of odd numbers.
     */
    private int sumOdd;
    /**
     * Store the sum of even numbers.
     */
    private int sumEven;
    /**
     * Creating object scanner which give opportunity to input values
     * direct in console.
     */
    private Scanner scanner = new Scanner(System.in);

    /**
     * Method which show odd numbers and even numbers in reverse order between
     * the numbers which are inputted by user in console.
     */
    public void showOddsAndEvens() {
        System.out.print("Input the first number: ");
        firstNumber = scanner.nextInt();
        System.out.print("Input the last number: ");
        lastNumber = scanner.nextInt();

        System.out.print("The odd numbers are: ");
        for (int i = firstNumber; i < lastNumber; i++) {
            if (i % 2 != 0) {
                sumOdd += i;
                System.out.print(i + " ");
            }
        }

        System.out.print("\nThe even numbers are: ");
        for (int i = lastNumber; i > firstNumber; i--) {
            if (i % 2 == 0) {
                sumEven += i;
                System.out.print(i + " ");
            }
        }

        System.out.println("\nThe odd sum is: " + sumOdd);
        System.out.println("The even sum is: " + sumEven);
    }
}
