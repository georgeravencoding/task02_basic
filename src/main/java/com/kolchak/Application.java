package com.kolchak;

/**
 * Class with logic where calculating Fibonacci numbers by inputting the count
 * of values in console by user.
 */

import com.kolchak.entity.FibonacciNumber;

/**
 * Class with logic where show odd numbers and even numbers in reverse order
 * between the numbers which are inputted by user in console.
 */
import com.kolchak.entity.OddsAndEvens;

/**
 * The Application program implements an application that
 * displays odd numbers and even numbers in reverse order
 * between the numbers which are inputted by user in console
 * and calculating Fibonacci numbers by inputting the count
 * of values in console by user.
 * <p>
 * Runner class.
 *
 * @author Maksym Kolchak
 * @version 1.0
 */
public class Application {
    public static void main(String[] args) {
        OddsAndEvens oddsAndEvens = new OddsAndEvens();
        oddsAndEvens.showOddsAndEvens();
        System.out.println("========================");
        FibonacciNumber fibonacciNumber = new FibonacciNumber();
        fibonacciNumber.showFibonacci();
    }
}

